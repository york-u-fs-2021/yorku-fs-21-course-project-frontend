//Contact Page

function validateN(form) {
    let nameError = document.getElementById('nameError') ;
        if (form.name.value == "" ) {
            nameError.innerHTML = "Please enter your name" ;
        } else {
            nameError.innerHTML = "" ;
        };
  };
  
  function validateE(form){
    let emailError = document.getElementById('emailError') ;
    let atpos = document.getElementById('email').value.indexOf("@") ;
        if (form.email.value == "") {
            emailError.innerHTML = "Please enter your email" ;
        } else if (atpos<1) {
            emailError.innerHTML = "Please enter a valid email" ;
        } else {
            emailError.innerHTML = "" ;
        };
  };
  
  function finalValidate() {
    let nameError = document.getElementById('nameError') ;
    let emailError = document.getElementById('emailError') ;
    let formError = document.getElementById('formError') ;
    if (nameError.innerHTML != "" || emailError.innerHTML != "" || document.getElementById('message').value == "") {
        event.preventDefault();
        formError.innerHTML = "Please fill out all fields" ;
    } else {
      alert("Thank you for your message.");
    };
  };