# YorkU CSFS 2021 Program Project - Frontend

## Description

This is the frontend for my personal portfolio website, created using React JS. Make sure to also check out its corresponding [backend](https://gitlab.com/york-u-fs-2021/yorku-fs-21-course-project-backend) and [database](https://gitlab.com/york-u-fs-2021/yorku-fs-21-course-project-database) repos.

A previous version of this project before it was converted to React (i.e. using basic html, css, and js files) can be viewed [here](https://gitlab.com/steve-dave8/csfs-individual-project).

### Live website: <http://www.stevenwhite.me/>


## Local Dev Environment

This project requires setting the variable `REACT_APP_API` whose value should be the url for the backend. For running locally, the value can be something like `http://localhost:4000` depending on which port you set for the backend.

### Dependencies

After cloning this project, in your Terminal (while in the frontend folder) run the command "npm install" which will create a node_modules folder that contains the project's dependencies as outlined in the package.json file. 

### Start Script

You will need to use one of the following:

For MacOS: `REACT_APP_API=http://localhost:4000 npm start`

For Windows (command line; cmd.exe): `set "REACT_APP_API=http://localhost:4000" && npm start`

For Windows (powershell): `($env:REACT_APP_API = "http://localhost:4000") -and (npm start)`

For more information on React & Environment Variables, see [documentation](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#adding-temporary-environment-variables-in-your-shell)

## Deployment

Continuous deployment has been set up for this frontend project using GitLab CI/CD variables. When changes are pushed/merged to the master branch they will be automatically deployed provided the CI pipeline passes (see the .gitlab-ci.yml files for details).


