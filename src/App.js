import React, { useState, useEffect } from 'react'
import './App.css'
import Navigation from './components/shared/Navigation'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import About from './components/pages/About'
import Resume from './components/pages/Resume'
import Portfolio from './components/pages/Portfolio'
import Contact from './components/pages/Contact'
import AdminPanel from './components/pages/AdminPanel'
import Login from './components/pages/Login'
import PrivateRoute from './components/shared/PrivateRoute'
import Footer from './components/shared/Footer'

function App() {
  const [token, setToken] = useState(false)
  const [mainMinHeight, setMainMinHeight] = useState("") //used for setting minimum height of smaller page components to ensure the footer goes to the bottom of the page

  // eslint-disable-next-line
  useEffect(() => {
    let status = sessionStorage.getItem('token')
    setToken(status)
  })

  const updateMainMinHeight = () => {
    const navHeight = document.getElementById("banner").clientHeight
    const footerHeight = document.getElementById("page-footer").clientHeight
    setMainMinHeight(`calc(100vh - ${navHeight}px - ${footerHeight}px - 10px)`) //subtract 10px for <main> padding
  }

  useEffect(() => {updateMainMinHeight()}, [])

  useEffect(() => {
    window.addEventListener("orientationchange", updateMainMinHeight)
    return () => {
      window.removeEventListener("orientationchange", updateMainMinHeight)
    }
  }, [])

  return (
    <BrowserRouter>
        <Navigation token={token} setToken={setToken}/>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about-me" component={About} />
          <Route exact path="/resume" component={Resume} />
          <Route exact path="/portfolio">
            <Portfolio mainMinHeight={mainMinHeight} />
          </Route>
          <Route exact path="/contact">
            <Contact mainMinHeight={mainMinHeight} />  
          </Route>   
          <PrivateRoute path="/admin-panel">
            <AdminPanel />
          </PrivateRoute>
          <Route exact path="/login">  
            <Login setToken={setToken} mainMinHeight={mainMinHeight} />
          </Route>
        </Switch>
        <Footer/>
    </BrowserRouter>
  )
}

export default App;
