const styles = {
    figure: {
        display: "grid", 
        justifyContent: "center",
        justifyItems: "center"
    },
    image: {
        maxWidth: "100%",
        maxHeight: "60vh"
    },
    caption: {
        textAlign: "center", 
        wordSpacing: "2px", 
        padding: "10px"
    }
}

const FetchError = ({ imgData, errCode }) => {
    return (
        <figure style={styles.figure}>
            <img src={imgData.image} alt={imgData.altText} style={styles.image} />
            <figcaption style={styles.caption} >
                Something went wrong. Please try again later.
                <br/>
                Error code: {errCode || "not available"}
            </figcaption>
        </figure>
    )
}

export default FetchError