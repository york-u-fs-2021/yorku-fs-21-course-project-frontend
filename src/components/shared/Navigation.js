import React, { useEffect, useState } from 'react'
import { NavLink } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'
import { useHistory } from "react-router-dom"
import "../../styles/navigation.css"

let timeout

const Navigation = (props) => {
    const [navOpen, setNavOpen] = useState(false)

    let history = useHistory()

    const logout = event => {
        event.preventDefault()
        sessionStorage.removeItem('token')
        props.setToken(false)
        setNavOpen(!navOpen)
        history.push("/")
        function stopTimeout(){
            clearTimeout(timeout)
        }
        stopTimeout()
    }

    useEffect(() => {
        const closeOnResize = () => {
            if (navOpen && window.innerWidth > 900) setNavOpen(false)
        }
        window.addEventListener("resize", closeOnResize)
        return () => {
            window.removeEventListener("resize", closeOnResize)
        }
    }, [navOpen]) 

    if (props.token && !timeout){
        function startTimeout(){
            timeout = setTimeout(function (){
                sessionStorage.removeItem('token')         
                history.push("/login")
                props.setToken(false)   
                setTimeout(function(){
                    alert("Sorry, your session has timed out. You have been logged out and returned to the login page.")
                }, 1000) //Add small delay to alert to ensure previous lines run and complete first.      
            }, 1000*60*60*2) //token expires after two hours
        }
        startTimeout()
        /*The token expires after two hours. While protected routes and their data will no longer be accessible the associated pages
        will still be visible. This timeout will remove them from view. Not the best for UX but I think it gives more security.
        If I knew how to do it I could refresh the token until a certain length of inactivity from the user is reached.*/
    }

    return (
        <header id="banner">
            <h1>Steven White</h1>
            <nav id="navbar-grid">
                <div className={`hamburger ${navOpen ? "open-hamburger" : ""}`} onClick={() => {setNavOpen(!navOpen)}}>
                    <i className="fas fa-bars"></i>
                </div>
                <div id="nav-btns-wrapper" className={navOpen ? "responsive" : ""}>
                    <div>
                        <NavLink tag={RouteLink} to="/">
                            <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>Home</button>
                        </NavLink>
                    </div>
                    <div>
                        <NavLink tag={RouteLink} to="/about-me">
                            <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>About Me</button>
                        </NavLink>
                    </div>
                    <div>
                        <NavLink tag={RouteLink} to="/resume">
                            <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>Resume</button>
                        </NavLink>                   
                    </div>   
                    <div>
                        <NavLink tag={RouteLink} to="/portfolio">
                            <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>Portfolio</button>
                        </NavLink>               
                    </div>
                    <div>
                        <NavLink tag={RouteLink} to="/contact">
                            <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>Contact</button>
                        </NavLink>                 
                    </div>      
                    {props.token
                        ? (<>
                            <div className="private">
                                <NavLink tag={RouteLink} to="/admin-panel">
                                    <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>Admin</button>
                                </NavLink>
                            </div>
                            <div className="private">
                                <NavLink tag={RouteLink} to="/">
                                    <button className="topnav" onClick={logout}>Logout</button>
                                </NavLink>                            
                            </div></>)
                        : (<div>
                                <NavLink tag={RouteLink} to="/login">
                                    <button className="topnav" onClick={() => {setNavOpen(!navOpen)}}>Login</button> 
                                </NavLink>                           
                            </div>)
                    }     
                </div>               
            </nav>
        </header>
    )
}

export default Navigation  