import React from 'react'
import '../../styles/footer.css'

const Footer = () => {
    return (
        <footer id="page-footer">
            <section id="footer-txt">
                <p>This website was deployed using Google Cloud Platform (GCP).</p>
            </section>
            <div id="footer-divider"></div>
            <section id="footer-social"> 
                <a target="_blank" rel="noopener noreferrer" href="https://codepen.io/steve-dave108">
                    <i className="fab fa-codepen"></i>
                </a>          
                <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/stevendavidwhite/">
                    <i className="fab fa-linkedin"></i>
                </a>           
                <a target="_blank" rel="noopener noreferrer" href="https://github.com/steve-dave8">
                    <i className="fab fa-github-square"></i>
                </a>           
                <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/steve-dave8">
                    <i className="fab fa-gitlab"></i>
                </a>                
            </section>
        </footer>
    )
}

export default Footer