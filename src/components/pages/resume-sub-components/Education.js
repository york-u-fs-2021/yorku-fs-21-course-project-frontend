import React, { useState, useEffect, useRef } from 'react'
import '../../../styles/AF-modal.css'
import '../../../styles/edu-slideshow.css'
import AFpopup from './AFpopup'
import EduSlideShow from './EducationSlideShow'
import Loading from '../../shared/Loading'
import FetchError from '../../shared/FetchError'
import { dataPiece } from '../assets/errorImages/error-img-data'

const renderEduPoints = (itemID, eduItemPoints) => {
    if (eduItemPoints.length){
        let bullets = eduItemPoints.filter(point => point.itemID === itemID)
        if (bullets.length){
            return (
                <ul className="educol1">
                    {bullets.map(bullet => {
                        return (<li key={bullet.pointID}>{bullet.point}</li>)
                    })}
                </ul>
            )
        }
    }
}

const Education = ({ loadingEdu, setLoadingEdu }) => {
    const [eduItems, setEduItems] = useState([])
    const [eduItemPoints, setEduItemPoints] = useState([])
    const [openLB, setOpenLB] = useState(false)
    const [slideIndex, setSlideIndex] = useState(0)
    const [error, setError] = useState({present: false, code: null})

    useEffect(() => {
        const getEduItemPoints = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/education-points', {
                method: 'GET',
                mode: 'cors'
            })
            try {
                const data = await response.json()
                setEduItemPoints(data)
            } catch {}
        }
        const getEduItems = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/education', {
                method: 'GET',
                mode: 'cors'
            })
            try {
                if (response.status === 200){
                    const data = await response.json()
                    setEduItems(data)
                } else {
                    setError({present: true, code: response.status})
                }
            } catch {
                setError({present: true, code: null})
            } finally {
                setLoadingEdu(false)
            }
        }
        getEduItemPoints()
        getEduItems()   
    }, [setLoadingEdu])

    // Modal for Animal Flow video:
    const popup = useRef()
    const openVideo = () => { popup.current.style.visibility = "visible" }

    // Lightbox for certificate slideshow:
    let offset = 0
    function incrementOffset() {
        offset++
        return null
    }

    function openLightbox(event) { 
        let index = parseInt(event.target.dataset.index) 
        // Note: this assumes there is an image for each education item. If there are items without an image then 
        // the index will be adjusted in the eduItems.map function below by using the offset variable above.
        setSlideIndex(index)
        setOpenLB(true)
    }

    return (
        <section>
            <h3 id="education" className="resume-h3" style={error.present ? {textAlign: "center"} : null} >Education</h3>
            {eduItems.length > 0 
                && eduItems.some(item => item.credentialName === "Certified Animal Flow Level 2 Instructor")
                && <AFpopup popupRef={popup} />
            }

            <div id="educolumns" style={error.present ? {gridTemplateColumns: "none"} : null} >   
                {loadingEdu
                    ?   <Loading type={'spinningBubbles'} color={'#8600b3'} containerClass={'loading loading--education'} />
                    :   error.present
                        ?   <FetchError imgData={dataPiece} errCode={error.code} />
                        :   (eduItems.length > 0) && eduItems.map((item, index) => { return (
                                <React.Fragment key={`item${item.itemID}`}>
                                    <div>
                                        <p className="educol1"><b>{item.credentialName}</b>
                                            {(item.credentialName === "Certified Animal Flow Level 2 Instructor")
                                                ? <button type="button" id="AFbutton" onClick={openVideo}>What is Animal Flow?</button>
                                                : <br/>
                                            }
                                            {item.place}
                                            <br/>
                                            {item.dateRange}
                                        </p>
                                        {renderEduPoints(item.itemID, eduItemPoints)}
                                    </div>
                                    <div>
                                        {item.image
                                            ? <img src={item.image} className="resGallery" alt={item.altText} data-index={index - offset} onClick={openLightbox}/>
                                            : incrementOffset()
                                        }
                                    </div>
                                </React.Fragment>
                            )})
                }
            </div>

            {eduItems.length > 0
                &&  <EduSlideShow 
                        eduItems={eduItems}
                        openLB={openLB}
                        setOpenLB={setOpenLB}
                        slideIndex={slideIndex}
                        setSlideIndex={setSlideIndex}
                    />
            }
        </section>
    )
}

export default Education