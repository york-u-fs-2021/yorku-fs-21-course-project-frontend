import AFvideo from '../../pages/assets/AnimalFlowLv2SW.mp4'

const AFpopup = ({ popupRef }) => {

    const closeVideo = () => { popupRef.current.style.visibility = "hidden" }

    return (
        <div id="AFpopup" ref={popupRef}>
            <figure id="AFvideo">           
                <div>
                    <div id="AFvideo-wrapper">
                        <div id="popupClose" onClick={closeVideo}>&times;</div>
                        <video controls preload="none">
                            <source src={AFvideo} type="video/mp4"/>
                            Sorry, this browser does not support this HTML5 video element.
                        </video>
                    </div>
                </div>
                <figcaption>
                    <p>Animal Flow is a ground-based movement system that is fun, challenging, and effective.
                    It is designed to improve strength, power, flexibility, mobility, and coordination for all levels of fitness enthusiasts.
                    </p>
                    <p>A common misconception is that it only involves moving like animals but there is much more to it than that.
                    In addition to animal locomotion, Animal Flow also takes inspiration from yoga, martial arts, 
                    breakdancing, hand balancing, parkour, and gymnastics among others.
                    </p>
                    <p>Here is the video I submitted for my level 2 certification. My flow had to be at least
                    3 minutes long and include all of the level 2 movements and most of the level 1 movements.
                    </p>
                </figcaption>
            </figure>
        </div>
    )
}

export default AFpopup