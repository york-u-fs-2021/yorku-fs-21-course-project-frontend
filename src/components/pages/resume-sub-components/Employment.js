import React, { useState, useEffect } from 'react'
import Loading from '../../shared/Loading'
import FetchError from '../../shared/FetchError'
import { dudeWhereData } from '../assets/errorImages/error-img-data'

const Employment = ({ loadingEmp, setLoadingEmp }) => {
    const [empItems, setEmpItems] = useState([])
    const [empItemPoints, setEmpItemPoints] = useState([])
    const [error, setError] = useState({present: false, code: null})

    useEffect(() => {
        const getEmpItemPoints = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/employment-points', {
                method: 'GET',
                mode: 'cors'
            })
            try {
                const data = await response.json()
                setEmpItemPoints(data)
            } catch {}
        }
        const getEmpItems = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/employment', {
                method: 'GET',
                mode: 'cors'
            })
            try {
                if (response.status === 200){
                    const data = await response.json()
                    setEmpItems(data)
                } else {
                    setError({present: true, code: response.status})
                }
            } catch {
                setError({present: true, code: null})
            } finally {
                setLoadingEmp(false)
            }
        }
        getEmpItemPoints()
        getEmpItems()  
    }, [setLoadingEmp])

    return (
        <section id="employment-section">
            <h3 id="employment" className="resume-h3" style={error.present ? {textAlign: "center"} : null} >Employment History</h3>
            {loadingEmp
                ?   <Loading type={'spinningBubbles'} color={'#8600b3'} containerClass={'loading'} />
                :   error.present
                    ?   <FetchError imgData={dudeWhereData} errCode={error.code} />
                    :   (empItems.length > 0) && empItems.map(item => { return (
                            <div key={item.itemID}>
                                <p><b>{item.jobPosition}</b><br/>{item.place}<br/>{item.dateRange}</p>
                                <ul>
                                    {empItemPoints.length > 0 && empItemPoints.filter(point => point.itemID === item.itemID)
                                        .map(point => {
                                            return (
                                                <li key={point.pointID}>{point.point}</li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        )})
            }
        </section>
    )
}

export default Employment