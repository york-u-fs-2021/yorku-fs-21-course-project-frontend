import { useEffect, useCallback } from "react"

const EduSlideShow = ({ eduItems, openLB, setOpenLB, slideIndex, setSlideIndex }) => {

    const slideshowLength = eduItems.filter(item => item.image).length

    const changeSlide = useCallback((n) => {
        const newIndex = slideIndex + n
        if (newIndex === slideshowLength) {
            setSlideIndex(0)
        } else if (newIndex < 0) {
            setSlideIndex(slideshowLength - 1)
        } else {
            setSlideIndex(newIndex)
        }
    }, [slideIndex, slideshowLength, setSlideIndex])

    useEffect(() => {
        function toggleSlideshow(event) {
            switch (event.key){
                case "ArrowLeft":
                case "Left":
                    changeSlide(-1)
                break
                case "ArrowRight":
                case "Right":
                    changeSlide(1)
                break
                case "Escape":
                case "Esc":
                    setOpenLB(false)
                break
                default:
                    //do nothing
            }
        }

        window.addEventListener('keydown', toggleSlideshow)
        
        return () => {
            window.removeEventListener('keydown', toggleSlideshow)
        }      
    }, [setOpenLB, changeSlide])

    return (
        <div id="Lightbox" className="modal" style={{display: openLB ? 'block' : 'none'}}>
            <div id="lightbox-wrapper">
                <span className="close pointer" onClick={() => setOpenLB(false)}>&times;</span>
                <div className="modal-content">
                    {(eduItems.length > 0) 
                        && eduItems.filter(item => item.image)
                            .map((item, index) => { return (
                                <div className="slide" key={item.itemID} style={{display: index === slideIndex ? 'block' : 'none'}}>
                                    <img className="image-slide" alt={item.altText} src={item.image}/>
                                </div>
                            )})
                    }
                    <button className="previous" onClick={() => changeSlide(-1)}>&#10094;</button>
                    <button className="next" onClick={() => changeSlide(1)}>&#10095;</button>
                </div>
            </div>
        </div>
    )
}

export default EduSlideShow