import React, { useState, useEffect } from 'react'
import Loading from '../../shared/Loading'
import FetchError from '../../shared/FetchError'
import { ifihaddata } from '../assets/errorImages/error-img-data'

const Awards = ({ loadingAwards, setLoadingAwards }) => {
    const [awardItems, setAwardItems] = useState([])
    const [error, setError] = useState({present: false, code: null})

    useEffect(() => {
        const getAwardItems = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/awards', {
                method: 'GET',
                mode: 'cors'
            })
            try {
                if (response.status === 200){
                    const data = await response.json()
                    setAwardItems(data)
                } else {
                    setError({present: true, code: response.status})
                }
            } catch {
                setError({present: true, code: null})
            } finally {
                setLoadingAwards(false)
            }
        }
        getAwardItems()
    }, [setLoadingAwards])

    return (
        <section id="awards-section">
            <h3 id="awards" className="resume-h3" style={error.present ? {textAlign: "center"} : null} >Awards</h3>
            {loadingAwards
                ?   <Loading type={'spinningBubbles'} color={'#8600b3'} containerClass={'loading'} />
                :   error.present 
                    ?   <FetchError imgData={ifihaddata} errCode={error.code} />
                    :   (awardItems.length > 0) && awardItems.map(item => { return (
                            <div key={item.itemID}>
                                <p>
                                    <b>{item.awardName}</b> ({item.year})
                                </p>
                                <ul>
                                    <li>{item.description}</li>
                                </ul>
                            </div>
                        )})
            }
        </section>
    )
}

export default Awards