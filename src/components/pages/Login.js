import React, { useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import "../../styles/login.css"

const Login = (props) => {
    const { mainMinHeight } = props
    let history = useHistory();
    let location = useLocation();
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const loginSubmit = async event => { 
        event.preventDefault()
        const response = await fetch(process.env.REACT_APP_API + '/auth', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({email, password})
        })
        const payload = await response.json()
        if (response.status === 401) {
            setAlertContent(payload)
        } else {
            sessionStorage.setItem('token', payload.token)
            let { from } = location.state || { from: { pathname: "/admin-panel" } }
            history.replace(from)
            let status = sessionStorage.getItem('token')
            props.setToken(status)
        }
    }

    return (
      <main style={{minHeight: mainMinHeight}}>
          <div id="login-wrapper">
          <form id="login-form" onSubmit={loginSubmit}>
            <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
            <div>
              <label htmlFor="usernameEntry">Email: </label>
              <input type="text" name="username" id="usernameEntry" required placeholder="Valid email address" value={email} onChange={e => setEmail(e.target.value)}/>
            </div>
            <div>
              <label htmlFor="passwordEntry">Password: </label>
              <input type="password" name="password" id="passwordEntry" required placeholder="Valid password" onChange={e => setPassword(e.target.value)}/>
            </div>    
            <button type="submit" className="stylish-submit">Sign in</button>
          </form>
          </div>
      </main>
    )
}

export default Login