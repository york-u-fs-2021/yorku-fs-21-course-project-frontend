import React, { useState } from 'react'
import CreateUser from './admin-panels/CreateUser'
import Listings from './admin-panels/Listings'
import ManagePortfolio from './admin-panels/ManagePortfolio'
import ManageResume from './admin-panels/ManageResume'

const AdminPanel = () => {
    const [panel, setPanel] = useState(<Listings/>)

    return (
        <main id="admin-page">
            <nav id="admin-nav">
                <div onClick={() => setPanel(<Listings/>)} className={`${(panel.type.name === "Listings") ? "active-panel" : ""}`}>
                    <p>Messages</p>
                </div>
                <div onClick={() => setPanel(<CreateUser/>)} className={`${(panel.type.name === "CreateUser") ? "active-panel" : ""}`}>
                    <p>Create User</p>
                </div>
                <div onClick={() => setPanel(<ManagePortfolio/>)} className={`${(panel.type.name === "ManagePortfolio") ? "active-panel" : ""}`}>
                    <p>Manage Portfolio</p>
                </div>
                <div onClick={() => setPanel(<ManageResume/>)} className={`${(panel.type.name === "ManageResume") ? "active-panel" : ""}`}>
                    <p>Manage Resume</p>
                </div>
            </nav>
            <section id="admin-panel">
                {panel}
            </section>
        </main>
    )
}

export default AdminPanel