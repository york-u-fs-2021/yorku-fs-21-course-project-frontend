import React, { useState } from 'react'
import fancyBorder from "./assets/victorian-border.png"
import "../../styles/contact.css"

const Contact = ({ mainMinHeight }) => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [content, setContent] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        const response = await fetch(process.env.REACT_APP_API + '/contact_form/entries', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({name, email, phoneNumber, content})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert("Message received. Thank you for reaching out.")
            resetForm()
        }
    }

    const resetForm = () => {
        setName("")
        setEmail("")
        setPhoneNumber("")
        setContent("")
    }

    return (
        <main style={{minHeight: mainMinHeight}}>
            <div className="form-outer-wrapper">
                <h2>Send a Message</h2>
                <div id="form-inner-wrapper">
                    <p>Please fill out the form below to get in contact with me.
                        <br/><br/><i>Fields marked with * are required.</i>
                    </p>
                    <form onSubmit={formSubmit} id="contact-form">
                        <div>
                            <label htmlFor="name">*Name: </label>
                            <input id="name" type="text" name="name" placeholder="Enter your full name" required value={name} onChange={e => setName(e.target.value)}/>
                        </div>
                        <div>
                            <label htmlFor="email">*Email: </label>
                            <input id="email" type="email" name="email" placeholder="Valid email address"  required value={email} onChange={e => setEmail(e.target.value)}/>
                        </div>
                        <div>
                            <label htmlFor="phone">Phone Number: </label>          
                            <input type="phone" name="phone" id="phone" placeholder="7091112222" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}/>
                        </div>                       
                        <p id="phone-microcopy">must be a 10-digit number with no dashes, brackets, etc.</p>                       
                        <label htmlFor="message" style={{display: "block", marginBottom: "-5px"}}>*Message: </label>
                            <textarea id="message" rows="10" cols="50" maxLength="2000" name="message" required value={content} onChange={e => setContent(e.target.value)}></textarea>                       
                        <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                        {/*If alertContent is null the div would still take up space on the page which is why
                        I use the 'hidden' class to change display to none.*/}
                        <button type="submit" className="stylish-submit">Submit</button>
                    </form>                
                </div>  
                <img src={fancyBorder} alt="Victorian-style border for contact form" id="victorian-border"/>      
            </div>
        </main>
      )
    }

    export default Contact