import React from 'react'
import { NavLink } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'
import SWheadshot from './assets/SWheadshot2021.jpg'
import "../../styles/home.css"

const Home = () => {
    return(
        <main>
            <section id="introduction">
                <div>
                    <p>Welcome to my personal website!</p>
                    <p>My name is Steven David White. I was born and raised in Newfoundland, Canada.</p>
                    <p>
                        I am open to work most anywhere in Canada for roles that are related to
                        web development, fitness, administration, and/or chemistry.
                    </p>                
                </div>

                <img id="SWheadshot" src={SWheadshot} alt="Headshot of Steven White"></img>
            </section>

            <section className="columns">
                <div>
                    <p className="uline center">Key skills:</p>
                    <ul id="key-skills">
                        <li>organizational skills</li>
                        <li>adaptability; can quickly learn how to use new technologies</li>
                        <li>proficient with Microsoft Word, Excel, and Powerpoint</li>
                        <li>familiar with Trello, Asana, Zoom, and Slack</li>
                        <li>active listening and interpersonal skills from customer service experience</li>
                        <li>teaching skills from tutoring roles, public speaking, and instructing fitness classes. Able to effectively convey information to different audiences.</li>
                        <li style={{lineHeight: "1.6"}}>knowledge of and practice in these technologies: HTML, CSS, SCSS, JSON, JavaScript (JS), ECMAScript 6 (ES6), jQuery, React, Node, Express, RESTful APIs, MySQL, MongoDB, Git, Google Cloud Platform (GCP), Amazon Web Services (AWS), and deployment</li>
                    </ul>
                </div>

                <div>
                    <p className="uline center">Summary of education:</p>
                    <ul id="edu-summary">
                        <li>Certificate in in Full-Stack Web Development</li>
                        <li>Certified to teach Yoga, Animal Flow, and Tai Chi</li>
                        <li>Advanced Diploma in Chemical Engineering Technology</li>
                        <li>B.Sc. Hons. in Environmental Science (chemistry major)</li>
                    </ul>
                </div>

                <div>
                    <p className="uline center">Here are some things that I like:</p>
                    <ul id="likes">
                        <li>coding</li>
                        <li>fitness: yoga, animal flow, tai chi, qigong</li>
                        <li>sports: tennis, badminton, ping pong</li>
                        <li>videogames; mostly RPGs</li>
                        <li>anime</li>
                        <li>cats</li>
                        <li>philosophy and mythology, especially as it relates to yoga</li>                     
                        <li>drinks: coffee, tea, americano, sake, gin</li>
                        <li>food: pizza, sushi, stir fry, seafood, jiggs dinner</li>
                    </ul>
                </div>
            </section>

            <p style={{margin: "2rem 0", textAlign: "center"}}>
                For more information, please see the{" "} 
                <NavLink tag={RouteLink} to="/about-me">About Me</NavLink>
                {" "}and{" "} 
                <NavLink tag={RouteLink} to="/resume">Resume</NavLink>
                {" "}pages.
            </p>
        </main>
    )
}

export default Home