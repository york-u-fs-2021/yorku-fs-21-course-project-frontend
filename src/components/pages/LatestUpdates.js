import "../../styles/latest-updates.css"

const LatestUpdates = () => {
    return (
        <section id="latest-updates">
            <h3 className="center">Latest Updates</h3>
            <div>
                <p className="update">
                    I've finished moving my main content to GCP. I have a new laptop and, while it has taken some time to reinstall software, it runs much 
                    quicker than my old one. I look forward to using it for more coding. As I get time between work and job searching, I'll return to a small project 
                    I was working on to learn more about Angular.
                </p>
                <p className="date-stamp">
                    - September 12, 2022
                </p>
            </div>
            <div></div>
            <div>
                <p className="update">
                    My previous domain name seems to have expired and I can't get access to it. At the same time, my AWS free trial has expired. 
                    I've registered a new but similar domain name and I'm working on moving all my content to GCP. Some content will be unavailable 
                    until I have finished transferring everything.
                </p>
                <p className="date-stamp">
                    - September 2, 2022
                </p>
            </div>
        </section>
    )
}

export default LatestUpdates