import WhereDataMeme from "./dude--wheres-my-data.png";
import IfIHadData from "./if-I-had-data.jpg";
import MissingDataPiece from "./data-piece.jpg";

class ImgData {
    constructor (image, altText) {
        this.image = image;
        this.altText = altText;
    }
}

const dudeWhereData = new ImgData(WhereDataMeme, "Meme which says 'Dude! Where's my data?'");
const ifihaddata = new ImgData(IfIHadData, "Meme which says 'This is where I'd put my data, if I had any.'");
const dataPiece = new ImgData(MissingDataPiece, "Meme which says 'Sometimes what a person needs is just one piece...' and a person is depicted holding a piece that says 'Data'.");

export { dudeWhereData, ifihaddata, dataPiece }