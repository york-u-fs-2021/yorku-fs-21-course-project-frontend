import React, { useEffect, useRef, useState } from 'react'
//Component imports:
import Employment from './resume-sub-components/Employment'
import Education from './resume-sub-components/Education'
import Awards from './resume-sub-components/Awards'

import '../../styles/resume.css'

import { debounce } from '../../helpers/util'


function offset(el) {
    if (!el) return
    let rect = el.getBoundingClientRect()
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop
    return rect.top + scrollTop
}

const Resume = () => {
    const rn1 = useRef()
    const rn2 = useRef()
    const rn3 = useRef()
    const divider1 = useRef()
    const divider2 = useRef()

    const [loadingEdu, setLoadingEdu] = useState(true)
    const [loadingEmp, setLoadingEmp] = useState(true)
    const [loadingAwards, setLoadingAwards] = useState(true)

    function changeColor() {
        let breakpoint1 = offset(divider1.current)
        let breakpoint2 = offset(divider2.current) - window.innerHeight + 50 //make adjustment because this section is smaller
    
        if (window.pageYOffset < breakpoint1) {
            rn1.current.className = "highlight"
        } else {
            rn1.current.className = ""
        }
        if (window.pageYOffset < breakpoint2 && window.pageYOffset > breakpoint1) {
            rn2.current.className = "highlight"
        } else {
            rn2.current.className = ""
        }
        if (window.pageYOffset > breakpoint2) {
            rn3.current.className = "highlight"
        } else {
            rn3.current.className = ""
        }
    }

    const debouncedChangeColor = debounce(changeColor, 15)

    useEffect(() => {
        window.addEventListener('scroll', debouncedChangeColor)
        window.addEventListener('resize', debouncedChangeColor)
        return () => {
            window.removeEventListener('scroll', debouncedChangeColor)
            window.removeEventListener('resize', debouncedChangeColor)
        }
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        //once data has finished loading then change the color of the side navigation bar
        if (!loadingEdu && !loadingEmp && !loadingAwards){
            setTimeout(changeColor, 300)
        }
    }, [loadingEdu, loadingEmp, loadingAwards])

    return (
        <>
            <nav id="resumenav">
                <ul>
                    <a className="reslink" href="#education"><li id="rn1" ref={rn1}>Education</li></a>
                    <a className="reslink" href="#employment"><li id="rn2" ref={rn2}>Employment <br/>History</li></a>                  
                    <a className="reslink" href="#awards"><li id="rn3" ref={rn3}>Awards</li></a>
                </ul>
            </nav>
            <main>
                <h2>Resume</h2>
                <p style={{margin: "0 10vw"}}>Note: you can click on a certificate image to open up a slideshow. Use the side navigation 
                bar to quickly jump between resume sections.</p>
                <Education loadingEdu={loadingEdu} setLoadingEdu={setLoadingEdu} />            
                <hr ref={divider1}/>
                <Employment loadingEmp={loadingEmp} setLoadingEmp={setLoadingEmp} />
                <hr ref={divider2}/>
                <Awards loadingAwards={loadingAwards} setLoadingAwards={setLoadingAwards} />
            </main>
        </>
    )
}

export default Resume