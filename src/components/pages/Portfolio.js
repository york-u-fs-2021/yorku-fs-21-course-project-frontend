import React, {useState, useEffect} from 'react'
import '../../styles/portfolio.css'
import Loading from '../shared/Loading'
import FetchError from '../shared/FetchError'
import { dudeWhereData } from './assets/errorImages/error-img-data'

const Portfolio = ({ mainMinHeight }) => {
    const [portfolio, setPortfolio] = useState([])
    const [items, setItems] = useState([])
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState({present: false, code: null})

    const getPortfolio = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/portfolio', {
            method: 'GET',
            mode: 'cors'
        })
        try {
            if (response.status === 200){
                const data = await response.json()
                setPortfolio(data)
            } else {
                setError({present: true, code: response.status})
            } 
        } catch {
            setError({present: true, code: null})
        } finally {
            setLoading(false)
        }
    }

    const getPFitems = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/portfolio-items', {
            method: 'GET',
            mode: 'cors'
        })
        try {
            const data = await response.json()
            data.reverse() //so latest items will show first
            setItems(data)
        } catch {}
    }

    useEffect(() => {
        getPFitems()
        getPortfolio()
    }, [])

    return (
        <main style={{minHeight: mainMinHeight}}>
            <h2>Portfolio</h2>

            {loading
                ?   <Loading type={'bubbles'} color={'#8600b3'} containerClass={'loading'} />
                :   error.present 
                    ?   <FetchError imgData={dudeWhereData} errCode={error.code} />
                    :   (portfolio.length > 0) && portfolio.map(section => { return (
                            <div key={section.sectionID} className="portfolio-section">
                                <h3 className='center'>{section.sectionTitle}</h3>
                                <p>{section.sectionDescription}</p>
                                <div className="portfolio-items">
                                    {items.filter(item => item.sectionID === section.sectionID)
                                        .map(item => {
                                            return (
                                            <div key={item.itemID}>
                                                <h4 className="item-title">{item.itemTitle}</h4>
                                                <p className="item-description">{item.itemDescription}</p>
                                                <div className="portfolio-img-container">
                                                    {item.screenshot &&
                                                        <img src={item.screenshot} alt={item.altText || ""} />
                                                    }
                                                </div>
                                                <div className="item-links">
                                                    {item.codeLink &&
                                                        <a href={item.codeLink} target="_blank" rel="noopener noreferrer">
                                                            <button type="button" className="btn-base code-btn">Code</button>
                                                        </a>
                                                    }
                                                    {item.demoLink &&
                                                        <a href={item.demoLink} target="_blank" rel="noopener noreferrer">
                                                            <button type="button" className="btn-base demo-btn">Demo</button>
                                                        </a>
                                                    }
                                                </div>
                                            </div>)
                                        }
                                    )}
                                </div>
                            </div>
                        )})
            }
        </main>
    )
}

export default Portfolio