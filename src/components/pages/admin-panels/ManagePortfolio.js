import React, { useState, useEffect } from 'react'
import { Table } from 'reactstrap'

import AddPFsection from './portfolio-components/AddPFsection'
import EditPFsection from './portfolio-components/EditPFsection'
import AddPFitem from './portfolio-components/AddPFitem'
import EditPFitem from './portfolio-components/EditPFitem'

const ManagePortfolio = () => {
    const token = sessionStorage.getItem('token')
    
    const [portfolio, setPortfolio] = useState([])
    const [section, setSection] = useState({})
    const [items, setItems] = useState([])
    const [itemPick, setItemPick] = useState({})

    const getPortfolio = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/portfolio', {
                method: 'GET',
                mode: 'cors'
            })
        const data = await response.json()
        setPortfolio(data)
    }

    const deletePFsection = async(event) => {
        let sectionID = event.target.dataset.key
        let confirmation = window.confirm("Deleting a portfolio section will also delete all of its contents. Deleted items cannot be retrieved. Are you sure you want to proceed?")
        if (confirmation){
            await fetch(`${process.env.REACT_APP_API}/portfolio/${sectionID}`, {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            getPortfolio()
        }
    }

    const getPFsection = async(event) => {
        let sectionID = event.target.dataset.key
        const response = await fetch(`${process.env.REACT_APP_API}/portfolio/${sectionID}`, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const data = await response.json()
        setSection(data)
    }

    const getPFitems = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/portfolio-items', {
            method: 'GET',
            mode: 'cors'
        })
        const data = await response.json()
        setItems(data)
    }

    const deletePFitem = async(event) => {
        let itemID = event.target.dataset.key
        let confirmation = window.confirm("Deleted items cannot be retrieved. Are you sure you want to proceed?")
        if (confirmation){
            await fetch(`${process.env.REACT_APP_API}/portfolio-items/${itemID}`, {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            getPFitems()
        }
    }

    const getPFitem = async(event) => {
        let itemID = event.target.dataset.key
        const response = await fetch(`${process.env.REACT_APP_API}/portfolio-items/${itemID}`, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const data = await response.json()
        setItemPick(data)
    }

    function findSectionTitle(item){
        let section = portfolio.find(section => section.sectionID === item.sectionID)
        return section.sectionTitle
    }

    useEffect(() => {
        getPortfolio()
        getPFitems()
    }, [])

    return (
        <>
            <h2>Portfolio Sections</h2>
            <AddPFsection getPortfolio={getPortfolio}/>
            <Table responsive id="portfolio-sections">
                <thead>
                    <tr>
                        <th style={{padding: "0 10px"}}>Section ID</th>
                        <th style={{padding: "0 10px"}}>Order</th>
                        <th>Title</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {portfolio.map(section => (
                        <tr key={section.sectionID}>
                            <td style={{textAlign: "center"}}>{section.sectionID}</td>
                            <td style={{textAlign: "center"}}>{section.order}</td>
                            <td>{section.sectionTitle}</td>
                            <td><button type="button" data-key={section.sectionID} onClick={e => getPFsection(e)}>Edit/View</button> | <button type="button" data-key={section.sectionID} onClick={e => deletePFsection(e)}>Delete</button></td>
                        </tr>)
                    )}
                </tbody>
            </Table>
            <EditPFsection section={section} setSection={setSection} getPortfolio={getPortfolio}/>
            <br/>
            <hr style={{width: "95%"}}/>
            <h2>Portfolio Items</h2>
            <AddPFitem portfolio={portfolio} getPFitems={getPFitems}/>
            <Table responsive id="portfolio-items">
                <thead>
                    <tr>
                        <th>Item Section</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {items.map(item => (
                        <tr key={item.itemID}>
                            <td style={{maxWidth: "200px"}}>{findSectionTitle(item)}</td>
                            <td>{item.itemTitle}</td>
                            <td style={{maxWidth: "350px"}}>{item.itemDescription}</td>
                            <td><button type="button" data-key={item.itemID} onClick={e => getPFitem(e)}>Edit/View</button> | <button type="button" data-key={item.itemID} onClick={e => deletePFitem(e)}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <EditPFitem itemPick={itemPick} setItemPick={setItemPick} portfolio={portfolio} getPFitems={getPFitems}/>
        </>
    )
}

export default ManagePortfolio