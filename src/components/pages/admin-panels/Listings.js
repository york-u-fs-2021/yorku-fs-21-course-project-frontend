import React, { useEffect, useState } from 'react'
import { Row, Table } from 'reactstrap'

const Listings = () => {
    const token = sessionStorage.getItem('token')
    
    useEffect(() => {
        (async () => {
            const response = await fetch(process.env.REACT_APP_API + '/contact_form/entries', {
                method: 'GET',
                mode: 'cors',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            const data = await response.json()
            setListing(data)
        })()
    }, [token])

    const [listing, setListing] = useState([]) 

    return (
        <>
            <Row>
                <h2>Listings for contact form submissions:</h2>
            </Row>
            {listing.length
                ? <Table responsive id="submissions" style={{margin: "0 1rem"}}>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listing.map(entry => <tr key={entry.messageID}><td>{entry.messageID}</td><td>{entry.name}</td><td>{entry.phoneNumber}</td><td>{entry.email}</td><td>{entry.content}</td></tr>)}
                    </tbody>
                </Table>
                : <p style={{textAlign: "center"}}>No messages found.</p>
            }           
            <br/>
        </>
    )
}

export default Listings