import React, { useState, useEffect } from 'react'

const EditPFsection = (props) => {
    const token = sessionStorage.getItem('token') 

    useEffect(() => {
        setSectionTitle(props.section.sectionTitle)
        setSectionDescription(props.section.sectionDescription)
        setOrder(props.section.order)
    }, [props.section])
    
    const [sectionTitle, setSectionTitle] = useState()
    const [sectionDescription, setSectionDescription] = useState()
    const [order, setOrder] = useState()
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        let sectionID = event.target.dataset.key
        const response = await fetch(`${process.env.REACT_APP_API}/portfolio/${sectionID}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({sectionTitle, sectionDescription, order})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            props.setSection({})
            props.getPortfolio()
        }      
    }

    return (
        <div id="edit-PF-section-modal" className={`${!props.section.sectionID ? "hidden" : ""}`}>
            <span className="close pointer" onClick={() => props.setSection({})}>×</span>
            <div className="PF-modal-content">
                <form onSubmit={formSubmit} data-key={props.section.sectionID}>
                    <p>Edit portfolio section:</p>
                    <p style={{fontStyle: "italic"}}>All fields are required</p>          
                    <label htmlFor="titleEntry">Section Title: </label>            
                        <input style={{width: "70%"}} type="text" name="title" id="titleEntry" required value={sectionTitle} onChange={e => setSectionTitle(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="orderEntry">Order: </label>            
                        <input style={{width: "5rem"}} type="number" min="1" name="order" id="orderEntry" required value={order} onChange={e => setOrder(parseInt(e.target.value))}/>     
                    <br/><br/>
                    <label htmlFor="descriptionEntry">Description:{String.fromCharCode(160)} 
                        <textarea style={{verticalAlign: "text-top"}} rows="5" cols="50" name="description" id="descriptionEntry" required value={sectionDescription} onChange={e => setSectionDescription(e.target.value)}></textarea>         
                    </label>
                    <br/><br/>
                    <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                    <button type="submit" className="stylish-submit">Submit</button>         
                </form>
            </div>
        </div>
    )
}

export default EditPFsection