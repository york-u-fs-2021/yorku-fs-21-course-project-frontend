import React, { useState } from 'react'

const AddPFitem = (props) => {
    const token = sessionStorage.getItem('token')
    
    const [sectionID, setSectionID] = useState("")
    const [itemTitle, setItemTitle] = useState("")
    const [itemDescription, setItemDescription] = useState("")
    const [codeLink, setCodeLink] = useState("")
    const [demoLink, setDemoLink] = useState("")
    const [screenshot, setScreenshot] = useState("")
    const [altText, setAltText] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault() 
        let itemSection = props.portfolio.find(x => x.sectionID === parseInt(sectionID))
        if (!itemSection){
            setAlertContent("Cannot add item to section that does not exist.")
            return
        }
        const response = await fetch(process.env.REACT_APP_API + '/portfolio/items', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({itemTitle, itemDescription, sectionID, codeLink, demoLink, screenshot, altText})
        })
        const payload = await response.json()
        if (response.status === 400) { 
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert("Success! A new item has been added to the portfolio.")
            resetForm()
            props.getPFitems()
        }
    }

    const resetForm = () => {
        setItemTitle("")
        setItemDescription("")
        setCodeLink("")
        setDemoLink("")
        setScreenshot("")
        setAltText("")
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add a new item to the portfolio</p>
                <p style={{fontStyle: "italic"}}>* required</p>          
                <label htmlFor="sectionIDentry">*Section ID: </label>            
                    <input style={{width: "5rem"}} type="number" min="1" name="sectionID" id="sectionIDentry" required value={sectionID} onChange={e => setSectionID(e.target.value)}/>     
                <br/><br/>
                <div className="form-text-field">
                    <label htmlFor="titleEntry">*Item title:</label>            
                    <input type="text" name="title" id="titleEntry" required value={itemTitle} onChange={e => setItemTitle(e.target.value)}/>     
                </div>
                <label htmlFor="itemDescription">*Item description:{String.fromCharCode(160)}
                    <textarea style={{verticalAlign: "text-top"}} rows="5" cols="50" name="itemDescription" id="itemDescription" required value={itemDescription} onChange={e => setItemDescription(e.target.value)}></textarea>         
                </label>
                <br/><br/>
                <div className="form-text-field">
                    <label htmlFor="codeLink">Code link:</label>            
                    <input type="text" name="code-link" id="codeLink" value={codeLink} onChange={e => setCodeLink(e.target.value)}/>     
                </div>
                <div className="form-text-field">
                    <label htmlFor="demoLink">Demo link:</label>            
                    <input type="text" name="demo-link" id="demoLink" value={demoLink} onChange={e => setDemoLink(e.target.value)}/>     
                </div>
                <div className="form-text-field">
                    <label htmlFor="screenshot">Screenshot:</label>            
                    <input type="text" name="screenshot" id="screenshot" value={screenshot} onChange={e => setScreenshot(e.target.value)}/>     
                </div>
                <div className="form-text-field">
                    <label htmlFor="screenshotAltText">Alt Text for Screenshot:</label>            
                    <input type="text" name="altText" id="screenshotAltText" value={altText} onChange={e => setAltText(e.target.value)}/>     
                </div>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit" className="stylish-submit block-center">Submit</button>     
            </form>
        </div>
    )
}

export default AddPFitem
