import React, { useState } from 'react'

const AddPFsection = (props) => {
    const token = sessionStorage.getItem('token')
    
    const [sectionTitle, setSectionTitle] = useState("")
    const [sectionDescription, setSectionDescription] = useState("")
    const [order, setOrder] = useState(1)
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        const response = await fetch(process.env.REACT_APP_API + '/portfolio', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({sectionTitle, sectionDescription, order})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(payload)
            resetForm()
            props.getPortfolio()
        }
    }

    const resetForm = () => {
        setSectionTitle("")
        setSectionDescription("")
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add a new section to the portfolio</p>
                <p style={{fontStyle: "italic"}}>* required</p>          
                <label htmlFor="titleEntry">*Section Title: </label>            
                    <input style={{width: "50%"}} type="text" name="title" id="titleEntry" required value={sectionTitle} onChange={e => setSectionTitle(e.target.value)}/>     
                <br/><br/>
                <label htmlFor="orderEntry">*Order: </label>            
                    <input style={{width: "5rem"}} type="number" min="1" name="order" id="orderEntry" required value={order} onChange={e => setOrder(parseInt(e.target.value))}/>     
                <br/><br/>
                <label htmlFor="descriptionEntry">Description:{String.fromCharCode(160)}
                    <textarea style={{verticalAlign: "text-top"}} rows="5" cols="50" name="description" id="descriptionEntry" value={sectionDescription} onChange={e => setSectionDescription(e.target.value)}></textarea>         
                </label>
                <br/><br/>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit" className="stylish-submit">Submit</button>         
            </form>
        </div>
    )
}

export default AddPFsection