import React, { useState, useEffect } from 'react'

const EditPFitem = (props) => {
    const token = sessionStorage.getItem('token')
    
    const [sectionID, setSectionID] = useState("")
    const [itemTitle, setItemTitle] = useState("")
    const [itemDescription, setItemDescription] = useState("")
    const [codeLink, setCodeLink] = useState("")
    const [demoLink, setDemoLink] = useState("")
    const [screenshot, setScreenshot] = useState("")
    const [altText, setAltText] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    useEffect(() => {
        setItemTitle(props.itemPick.itemTitle)
        setItemDescription(props.itemPick.itemDescription)
        setSectionID(props.itemPick.sectionID)
        setCodeLink(props.itemPick.codeLink || "")
        setDemoLink(props.itemPick.demoLink || "")
        setScreenshot(props.itemPick.screenshot || "")
        setAltText(props.itemPick.altText || "")
    }, [props.itemPick])

    const formSubmit = async (event) => {
        event.preventDefault()
        let itemSection = props.portfolio.find(x => x.sectionID === parseInt(sectionID))
        if (!itemSection){
            setAlertContent("Cannot add item to section that does not exist.")
            return
        }
        let itemID = props.itemPick.itemID
        const response = await fetch(`${process.env.REACT_APP_API}/portfolio-items/${itemID}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({itemTitle, itemDescription, codeLink, demoLink, sectionID, screenshot, altText})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            props.setItemPick({})
            props.getPFitems()
        }      
    }

    return (
        <div id="edit-PF-item-modal" className={`${!props.itemPick.itemID ? "hidden" : ""}`}>
            <span className="close pointer" onClick={() => props.setItemPick({})}>×</span>
            <div className="PF-modal-content">
                <form onSubmit={formSubmit} style={{maxWidth: "80%"}}>
                    <p>Fill out this form to edit a portfolio item. You can also use this form to move an item to another section provided the section already exists.</p>
                    <p style={{fontStyle: "italic"}}>* required</p>          
                    <label htmlFor="sectionIDentry">*Section ID: </label>            
                        <input style={{width: "5rem"}} type="number" min="1" name="sectionID" id="sectionIDentry" required value={sectionID} onChange={e => setSectionID(parseInt(e.target.value))}/>     
                    <br/><br/>
                    <label htmlFor="titleEntry">*Item title: </label>            
                        <input style={{width: "50%"}} type="text" name="title" id="titleEntry" required value={itemTitle} onChange={e => setItemTitle(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="itemDescription">*Item description:{String.fromCharCode(160)} 
                        <textarea style={{verticalAlign: "text-top"}} rows="5" cols="50" name="itemDescription" id="itemDescription" required value={itemDescription} onChange={e => setItemDescription(e.target.value)}></textarea>         
                    </label>
                    <br/><br/>
                    <label htmlFor="codeLink">Code link: </label>            
                        <input style={{width: "50%"}} type="text" name="code-link" id="codeLink" value={codeLink} onChange={e => setCodeLink(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="demoLink">Demo link: </label>            
                        <input style={{width: "50%"}} type="text" name="demo-link" id="demoLink" value={demoLink} onChange={e => setDemoLink(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="edit-screenshot">Screenshot: </label>            
                        <input style={{width: "50%"}} type="text" name="screenshot" id="edit-screenshot" value={screenshot} onChange={e => setScreenshot(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="edit-altText">Alt text for screenshot: </label>            
                        <input style={{width: "50%"}} type="text" name="altText" id="edit-altText" value={altText} onChange={e => setAltText(e.target.value)}/>     
                    <br/><br/>
                    <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                    <button type="submit" className="stylish-submit">Submit</button>         
                </form>
            </div>
        </div>
    )
}

export default EditPFitem