import React, { useState } from 'react'

const CreateUser = () => {
    const token = sessionStorage.getItem('token')
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        const response = await fetch(process.env.REACT_APP_API + '/users', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name, email, password})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(`Success! User created for ${payload.name} with email address ${payload.email}`)
            resetForm()
        }
    }

    const resetForm = () => {
        setName("")
        setEmail("")
        setPassword("")
    }

    return (
        <div className="form-wrapper">
            <h2 style={{marginBottom: "8px"}}>Create A User</h2>
            <div id="createUser-form">
                <p>Fill out and submit the form below to add a new user to the system.</p>
                <form onSubmit={formSubmit}>
                    <p style={{fontStyle: "italic"}}>All fields are required</p>          
                    <label htmlFor="nameEntry">Name: </label>            
                        <input type="name" name="name" id="nameEntry" placeholder="Enter name" required value={name} onChange={e => setName(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="emailEntry" sm={1}>Email: </label>
                        <input type="email" name="email" id="emailEntry" placeholder="Valid email address"  required value={email} onChange={e => setEmail(e.target.value) }/>                 
                    <br/><br/>
                    <label htmlFor="passwordEntry" sm={1}>*Password: </label>
                        <input type="password" name="password" id="passwordEntry" value={password} onChange={e => setPassword(e.target.value)}/>          
                    <br/>
                    <p style={{fontSize: "80%", marginLeft: "2rem"}}>*password must be at least 8 characters long</p>
                    <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                    <button type="submit" className="stylish-submit">Submit</button>         
                </form>
                <br/><br/>
            </div>
        </div>
    )
}

export default CreateUser