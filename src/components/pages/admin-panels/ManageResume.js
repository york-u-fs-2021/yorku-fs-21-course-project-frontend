import React, { useEffect } from 'react'
import ManageEmployment from './resume-sections/ManageEmployment';
import ManageEducation from './resume-sections/ManageEducation';
import ManageAwards from './resume-sections/ManageAwards';

const ManageResume = () => {
    useEffect(() => {
        let accordion = document.getElementsByClassName("accordion");
        let i;
        for (i = 0; i < accordion.length; i++) {
            accordion[i].addEventListener("click", function() {
                /* Toggle between adding and removing the "active" class,
                to highlight the button that controls the panel */
                this.classList.toggle("activePanel");
                /* Toggle between hiding and showing the active panel */
                let panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        };
        window.onresize = function (){
            let activePanels = document.querySelectorAll(".activePanel + section.panel");
            for (let j = 0; j < activePanels.length; j++) {
                activePanels[j].style.maxHeight = activePanels[j].scrollHeight + "px";
            }
        } 
    });

    return (
        <>
            <br/>
            <button className="accordion resume-accordion">
                <h3 className="center">Employment History</h3>
            </button> 
            <section className="panel">
                <div className="panel-drawer">
                    <ManageEmployment/>
                </div>
            </section>
            <hr/>
            <button className="accordion resume-accordion">
                <h3 className="center">Education</h3>
            </button> 
            <section className="panel">
                <div className="panel-drawer">
                    <ManageEducation/>
                </div>
            </section>
            <hr/>
            <button className="accordion resume-accordion">
                <h3 className="center">Awards</h3>
            </button> 
            <section className="panel">
                <div className="panel-drawer">
                    <ManageAwards/>
                </div>
            </section>
            <hr/>
        </>
    )
}

export default ManageResume