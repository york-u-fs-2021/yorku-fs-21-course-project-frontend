import React, { useState, useEffect } from 'react'
import { Table } from 'reactstrap'
import AddAward from './award-components/AddAward'
import EditAward from './award-components/EditAward'

const ManageAwards = () => {
    const token = sessionStorage.getItem('token')

    const [awardItems, setAwardItems] = useState([])
    const [itemPick, setItemPick] = useState({})

    const getAwardItems = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/resume/awards', {
            method: 'GET',
            mode: 'cors'
        })
        const data = await response.json()
        setAwardItems(data)
    }

    const deleteAwardItem = async (event) => {
        let itemID = event.target.dataset.key
        let confirmation = window.confirm("Deleted items cannot be retrieved. Are you sure you want to proceed?")
        if (confirmation){
            await fetch(`${process.env.REACT_APP_API}/resume/awards/${itemID}`, {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            getAwardItems()
        }
    }

    const getAwardItem = async (event) => {
        let itemID = event.target.dataset.key
        const response = await fetch(`${process.env.REACT_APP_API}/resume/awards/${itemID}`, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const data = await response.json()
        setItemPick(data)
    }

    useEffect(() => {getAwardItems()}, [])

    return (
        <>
            <AddAward getAwardItems={getAwardItems}/>
            <Table responsive id="award-items">
                <thead>
                    <tr>
                        <th style={{padding: "0 10px"}}>Item ID</th>
                        <th>Award Name</th>
                        <th>Year(s)</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {awardItems.map(item => <tr key={item.itemID}><td style={{textAlign: "center"}}>{item.itemID}</td><td>{item.awardName}</td><td>{item.year}</td><td>{item.description}</td>
                    <td><i className="fas fa-edit" title="Edit" data-key={item.itemID} onClick={e => getAwardItem(e)}></i> | <i className="fas fa-trash-alt" title="Delete" data-key={item.itemID} onClick={e => deleteAwardItem(e)}></i></td>
                    </tr>)}
                </tbody>
            </Table>
            <EditAward itemPick={itemPick} setItemPick={setItemPick} getAwardItems={getAwardItems}/>
            <div style={{marginBottom: "1rem"}}></div>
        </>
    )
}

export default ManageAwards