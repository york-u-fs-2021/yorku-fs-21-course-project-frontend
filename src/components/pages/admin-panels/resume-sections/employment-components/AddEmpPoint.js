import React, { useState } from 'react'

const AddEmpPoint = (props) => {
    const token = sessionStorage.getItem('token')

    const [itemID, setItemID] = useState("")
    const [points, setPoints] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault() 
        let item = props.empItems.find(x => x.itemID === parseInt(itemID))
        if (!item){
            setAlertContent("Cannot add bullet point to item that does not exist.")
            return
        }
        const response = await fetch(process.env.REACT_APP_API + '/resume/employment-points', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({itemID, points})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(`Successfully added bullet point(s) to employment item with id: ${itemID}`)
            resetForm()
        }
    }

    const resetForm = () => {
        setItemID("")
        setPoints("")
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add one or more bullet points to an employment item. Separate bullet points by pressing the enter key once for each additional point.</p>       
                <label htmlFor="itemIDentry">Item ID: </label>            
                    <input style={{width: "5rem"}} type="number" min="1" name="itemID" id="itemIDentry" required value={itemID} onChange={e => setItemID(e.target.value)}/>     
                <br/><br/>
                <label htmlFor="bulletPoint">Bullet point(s):{String.fromCharCode(160)}
                    <textarea className="new-bullet-point" style={{verticalAlign: "text-top", width: "80%"}} rows="5" name="bulletPoint" id="bulletPoint" required value={points} onChange={e => setPoints(e.target.value)}></textarea>         
                </label>
                <br/><br/>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit" className="matrix-btn">Submit</button>         
            </form>
        </div>
    )
}

export default AddEmpPoint
