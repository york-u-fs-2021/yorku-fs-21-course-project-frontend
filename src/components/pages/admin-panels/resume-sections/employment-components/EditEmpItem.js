import React, { useState, useEffect } from 'react'

const EditEmpItem = (props) => {
    const token = sessionStorage.getItem('token')

    const [jobPosition, setJobPosition] = useState("")
    const [place, setPlace] = useState("")
    const [dateRange, setDateRange] = useState("")
    const [alertContent, setAlertContent] = useState(null)
    const [bulletPicks, setBulletPicks] = useState([])

    useEffect(() => {
        setJobPosition(props.itemPick.jobPosition)
        setPlace(props.itemPick.place)
        setDateRange(props.itemPick.dateRange)
    }, [props.itemPick])

    useEffect(() => {
        const getItemBullets = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/employment-points', {
                method: 'GET',
                mode: 'cors'
            })
            const data = await response.json()
            let bullets = data.filter(x => x.itemID === props.itemPick.itemID)
            setBulletPicks(bullets)
        }
        getItemBullets()
    }, [props.itemPick.itemID])

    const updateEmpItem = async () => {
        let itemID = props.itemPick.itemID
        const response = await fetch(`${process.env.REACT_APP_API}/resume/employment/${itemID}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({jobPosition, place, dateRange})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
            return false
        } else {
            setAlertContent(null)
            return true
        }
    }

    const deleteEmpPoint = async (id) => {
        fetch(`${process.env.REACT_APP_API}/resume/employment-points/${id}`, {
            method: 'DELETE',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
    }

    const deleteSelectedPoints = () => {
        let points = Array.from(document.getElementsByClassName("delete-emp-point"))
        points.forEach(point => {
            if (point.checked){
                let id = point.dataset.key
                deleteEmpPoint(id)
            }
        })
    }

    const updateEmpPoint = async (id, update) => {
        let point = update
        fetch(`${process.env.REACT_APP_API}/resume/employment-points/${id}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({point})
        })
    }

    const updatePoints = () => {
        let points = Array.from(document.getElementsByClassName("delete-emp-point"))
        points.forEach(point => {
            if (!point.checked){
                let id = point.dataset.key
                let update = point.parentElement.previousElementSibling.value
                updateEmpPoint(id, update)
            }
        })
    }

    const formSubmit = async (event) => {
        event.preventDefault()
        let updated = await updateEmpItem()
        if (!updated) return
        deleteSelectedPoints()
        updatePoints()
        setAlertContent(null)
        props.getEmpItems()
        props.setItemPick({}) 
    }

    return (
        <div id="edit-empItem-modal" className={`${!props.itemPick.itemID ? "hidden" : ""}`}>
            <span className="close pointer" onClick={() => props.setItemPick({})}>×</span>
            <div className="emp-modal-content">
                <form onSubmit={formSubmit}>
                    <p>Fill out this form to edit an employment item and its associated bullet points.</p>
                    <p style={{fontStyle: "italic"}}>All fields are required</p>          
                    <label htmlFor="editJob">Job Position: </label>            
                        <input style={{width: "50%"}} type="text" name="job" id="editJob" required value={jobPosition} onChange={e => setJobPosition(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="editPlace">Place (company, city, and province): </label>
                        <input style={{width: "50%"}} type="text" name="place" id="editPlace" required value={place} onChange={e => setPlace(e.target.value)}/>                       
                    <br/><br/>
                    <label htmlFor="editDate">Date Range: </label>
                        <input style={{width: "50%"}} type="text" name="date-range" id="editDate" required value={dateRange} onChange={e => setDateRange(e.target.value)}/>
                    <br/><br/>
                    <label>Bullet point(s):</label>
                    <br/>
                    {bulletPicks.map(bullet => {
                        return (
                            <>
                                <textarea className="edit-bullet-point" style={{width: "80%", verticalAlign: "middle"}} rows="3" name="bulletPoint" required defaultValue={bullet.point}></textarea>
                                <label htmlFor="delete-point"> Delete 
                                    <input className="delete-emp-point" type="checkbox" name="delete-point" data-key={bullet.pointID}/>
                                </label>                               
                            </>
                        )
                    })}
                    <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                    <br/>
                    <button type="submit" className="matrix-btn">Submit</button>         
                </form>
            </div>
        </div>
    )
}

export default EditEmpItem