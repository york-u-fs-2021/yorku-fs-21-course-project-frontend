import React, { useState } from 'react'

const AddEmpItem = (props) => {
    const token = sessionStorage.getItem('token')
    
    const [jobPosition, setJobPosition] = useState("")
    const [place, setPlace] = useState("")
    const [dateRange, setDateRange] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        event.persist()
        const response = await fetch(process.env.REACT_APP_API + '/resume/employment', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({jobPosition, place, dateRange})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(payload)
            resetForm()
            props.getEmpItems()
            resizePanel(event)
        }
    }

    const resetForm = () => {
        setJobPosition("")
        setPlace("")
        setDateRange("")
    }

    function resizePanel(event){
        let panel = event.target.closest("section.panel")
        panel.style.maxHeight = (panel.scrollHeight + 50) + "px";
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add a new item to the employment section of the resume.</p>
                <p style={{fontStyle: "italic"}}>All fields are required</p>          
                <label htmlFor="jobEntry">Job Position: </label>            
                    <input style={{width: "50%"}} type="text" name="job" id="jobEntry" required value={jobPosition} onChange={e => setJobPosition(e.target.value)}/>     
                <br/><br/>
                <label htmlFor="placeEntry">Place (company, city, and province): </label>
                    <input style={{width: "50%"}} type="text" name="place" id="placeEntry" required value={place} onChange={e => setPlace(e.target.value)}/>                       
                <br/><br/>
                <label htmlFor="dateEntry">Date Range: </label>
                    <input style={{width: "50%"}} type="text" name="date-range" id="dateEntry" required value={dateRange} onChange={e => setDateRange(e.target.value)}/>
                <br/><br/>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit" className="matrix-btn">Submit</button>         
            </form>
        </div>
    )
}

export default AddEmpItem