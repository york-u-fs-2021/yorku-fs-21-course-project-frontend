import React, { useState, useEffect } from 'react'
import { Table } from 'reactstrap'
import AddEmpItem from './employment-components/AddEmpItem'
import AddEmpPoint from './employment-components/AddEmpPoint'
import EditEmpItem from './employment-components/EditEmpItem'

const ManageEmployment = () => {
    const token = sessionStorage.getItem('token')

    const [empItems, setEmpItems] = useState([])
    const [itemPick, setItemPick] = useState({})

    const getEmpItems = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/resume/employment', {
            method: 'GET',
            mode: 'cors'
        })
        const data = await response.json()
        setEmpItems(data)
    }

    const deleteEmpItem = async (event) => {
        let itemID = event.target.dataset.key
        let confirmation = window.confirm("Deleted items cannot be retrieved. Are you sure you want to proceed?")
        if (confirmation){
            await fetch(`${process.env.REACT_APP_API}/resume/employment/${itemID}`, {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            getEmpItems()
        }
    }

    const getEmpItem = async (event) => {
        let itemID = event.target.dataset.key
        const response = await fetch(`${process.env.REACT_APP_API}/resume/employment/${itemID}`, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const data = await response.json()
        setItemPick(data)
    }

    useEffect(() => {getEmpItems()}, [])

    return (
        <>
            <AddEmpItem getEmpItems={getEmpItems}/>
            <AddEmpPoint empItems={empItems}/>
            <Table responsive id="employment-items">
                <thead>
                    <tr>
                        <th style={{padding: "0 10px"}}>Item ID</th>
                        <th>Job Position</th>
                        <th>Date Range</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {empItems.map(item => <tr key={item.itemID}><td style={{textAlign: "center"}}>{item.itemID}</td><td>{item.jobPosition}</td><td>{item.dateRange}</td>
                    <td><i className="fas fa-edit" title="Edit/View" data-key={item.itemID} onClick={e => getEmpItem(e)}></i> | <i className="fas fa-trash-alt" title="Delete" data-key={item.itemID} onClick={e => deleteEmpItem(e)}></i></td>
                    </tr>)}
                </tbody>
            </Table>
            <EditEmpItem itemPick={itemPick} setItemPick={setItemPick} getEmpItems={getEmpItems}/>
            <div style={{marginBottom: "1rem"}}></div>
        </>
    )
}

export default ManageEmployment