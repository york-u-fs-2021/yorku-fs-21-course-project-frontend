import React, { useState } from 'react'

const AddEduPoint = (props) => {
    const token = sessionStorage.getItem('token')

    const [itemID, setItemID] = useState("")
    const [points, setPoints] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault() 
        let item = props.eduItems.find(x => x.itemID === parseInt(itemID))
        if (!item){
            setAlertContent("Cannot add bullet point to item that does not exist.")
            return
        }
        const response = await fetch(process.env.REACT_APP_API + '/resume/education-points', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({itemID, points})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(`Successfully added bullet point(s) to education item with id: ${itemID}`)
            resetForm()
        }
    }

    const resetForm = () => {
        setItemID("")
        setPoints("")
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add one or more bullet points to an education item. Separate bullet points by pressing the enter key once for each additional point.</p>       
                <label htmlFor="eduItemIDentry">Item ID: </label>            
                    <input style={{width: "5rem"}} type="number" min="1" name="itemID" id="eduItemIDentry" required value={itemID} onChange={e => setItemID(e.target.value)}/>     
                <br/><br/>
                <label htmlFor="eduBulletPoint">Bullet point(s):{String.fromCharCode(160)}
                    <textarea className="new-bullet-point" style={{verticalAlign: "text-top", width: "80%"}} rows="5" name="bullet-point" id="eduBulletPoint" required value={points} onChange={e => setPoints(e.target.value)}></textarea>         
                </label>
                <br/><br/>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit">Submit</button>         
            </form>
        </div>
    )
}

export default AddEduPoint
