import React, { useState, useEffect } from 'react'

const EditEduItem = (props) => {
    const token = sessionStorage.getItem('token')

    const [credentialName, setCredentialName] = useState("")
    const [place, setPlace] = useState("")
    const [dateRange, setDateRange] = useState("")
    const [image, setImage] = useState("")
    const [altText, setAltText] = useState("")
    const [alertContent, setAlertContent] = useState(null)
    const [bulletPicks, setBulletPicks] = useState([])

    useEffect(() => {
        setCredentialName(props.itemPick.credentialName)
        setPlace(props.itemPick.place)
        setDateRange(props.itemPick.dateRange)
        setImage(props.itemPick.image)
        setAltText(props.itemPick.altText)
    }, [props.itemPick])

    useEffect(() => {
        const getItemBullets = async () => {
            const response = await fetch(process.env.REACT_APP_API + '/resume/education-points', {
                method: 'GET',
                mode: 'cors'
            })
            const data = await response.json()
            let bullets = data.filter(x => x.itemID === props.itemPick.itemID)
            setBulletPicks(bullets)
        }
        getItemBullets()
    }, [props.itemPick.itemID])

    const updateEduItem = async () => {
        let itemID = props.itemPick.itemID
        let response = await fetch(`${process.env.REACT_APP_API}/resume/education/${itemID}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({credentialName, place, dateRange, image, altText})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
            return false
        } else {
            setAlertContent(null)
            return true
        }
    }

    const deleteEduPoint = async (id) => {
        fetch(`${process.env.REACT_APP_API}/resume/education-points/${id}`, {
            method: 'DELETE',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
    }

    const deleteSelectedPoints = () => {
        let points = Array.from(document.getElementsByClassName("delete-edu-point"))
        points.forEach(point => {
            if (point.checked){
                let id = point.dataset.key
                deleteEduPoint(id)
            }
        })
    }

    const updateEduPoint = async (id, update) => {
        let point = update
        fetch(`${process.env.REACT_APP_API}/resume/education-points/${id}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({point})
        })
    }

    const updatePoints = () => {
        let points = Array.from(document.getElementsByClassName("delete-edu-point"))
        points.forEach(point => {
            if (!point.checked){
                let id = point.dataset.key
                let update = point.parentElement.previousElementSibling.value
                updateEduPoint(id, update)
            }
        })
    }

    const formSubmit = async (event) => {
        event.preventDefault()
        let updated = await updateEduItem()
        if (!updated) return
        deleteSelectedPoints()
        updatePoints()
        setAlertContent(null)
        props.getEduItems()
        props.setItemPick({}) 
    }

    return (
        <div id="edit-eduItem-modal" className={`${!props.itemPick.itemID ? "hidden" : ""}`}>
            <span className="close pointer" onClick={() => props.setItemPick({})}>×</span>
            <div className="edu-modal-content">
                <form onSubmit={formSubmit}>
                    <p>Fill out this form to edit an education item and its associated bullet points.</p>
                    <p style={{fontStyle: "italic"}}>Fields marked with * are required.</p>
                    <label htmlFor="editCredential">*Credential Name: </label>            
                        <input style={{width: "50%"}} type="text" name="credential-name" id="editCredential" required value={credentialName} onChange={e => setCredentialName(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="editEduPlace">*Place (school, city, and province): </label>
                        <input style={{width: "50%"}} type="text" name="place" id="editEduPlace" required value={place} onChange={e => setPlace(e.target.value)}/>                       
                    <br/><br/>
                    <label htmlFor="editEduDate">*Date Range: </label>
                        <input style={{width: "50%"}} type="text" name="date-range" id="editEduDate" required value={dateRange} onChange={e => setDateRange(e.target.value)}/>
                    <br/><br/>
                    <label htmlFor="editImage">Image: </label>
                        <input style={{width: "50%"}} type="text" name="image" id="editImage" value={image} onChange={e => setImage(e.target.value)}/>
                    <br/><br/>
                    <label htmlFor="editAltText">Image Alt Text: </label>
                        <input style={{width: "50%"}} type="text" name="alt-text" id="editAltText" value={altText} onChange={e => setAltText(e.target.value)}/>
                    <br/><br/>


                    <label>*Bullet point(s):</label>
                    <br/>
                    {bulletPicks.length
                        ? bulletPicks.map(bullet => {
                            return (
                                <>
                                    <textarea className="edit-bullet-point" style={{width: "80%", verticalAlign: "middle"}} rows="3" name="bulletPoint" required>{bullet.point}</textarea>
                                    <label htmlFor="delete-point"> Delete 
                                        <input className="delete-edu-point" type="checkbox" name="delete-point" data-key={bullet.pointID}/>
                                    </label>                               
                                </>
                            )})
                        : <p>This item does not have any bullet points.</p>
                    }
                    <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                    <br/>
                    <button type="submit">Submit</button>         
                </form>
            </div>
        </div>
    )
}

export default EditEduItem