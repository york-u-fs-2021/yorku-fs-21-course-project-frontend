import React, { useState } from 'react'

const AddEduItem = (props) => {
    const token = sessionStorage.getItem('token')
    
    const [credentialName, setCredentialName] = useState("")
    const [place, setPlace] = useState("")
    const [dateRange, setDateRange] = useState("")
    const [image, setImage] = useState("")
    const [altText, setAltText] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        event.persist()
        const response = await fetch(process.env.REACT_APP_API + '/resume/education', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({credentialName, place, dateRange, image, altText})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(payload)
            resetForm()
            props.getEduItems()
            resizePanel(event)
        }
    }

    const resetForm = () => {
        setCredentialName("")
        setPlace("")
        setDateRange("")
        setImage("")
        setAltText("")
    }

    function resizePanel(event){
        let panel = event.target.closest("section.panel")
        panel.style.maxHeight = (panel.scrollHeight + 50) + "px";
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add a new item to the education section of the resume.</p>
                <p style={{fontStyle: "italic"}}>Fields marked with * are required.</p>          
                <label htmlFor="credentialEntry">*Credential Name: </label>            
                    <input style={{width: "50%"}} type="text" name="credential-name" id="credentialEntry" required value={credentialName} onChange={e => setCredentialName(e.target.value)}/>     
                <br/><br/>
                <label htmlFor="eduPlaceEntry">*Place (school, city, and province): </label>
                    <input style={{width: "50%"}} type="text" name="place" id="eduPlaceEntry" required value={place} onChange={e => setPlace(e.target.value)}/>                       
                <br/><br/>
                <label htmlFor="eduDateEntry">*Date Range: </label>
                    <input style={{width: "50%"}} type="text" name="date-range" id="eduDateEntry" required value={dateRange} onChange={e => setDateRange(e.target.value)}/>
                <br/><br/>
                <label htmlFor="imageEntry">Image: </label>
                    <input style={{width: "50%"}} type="text" name="image" id="imageEntry" value={image} onChange={e => setImage(e.target.value)}/>
                <br/><br/>
                <label htmlFor="altText">Image Alt Text: </label>
                    <input style={{width: "50%"}} type="text" name="alt-text" id="altText" value={altText} onChange={e => setAltText(e.target.value)}/>
                <br/><br/>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit">Submit</button>         
            </form>
        </div>
    )
}

export default AddEduItem