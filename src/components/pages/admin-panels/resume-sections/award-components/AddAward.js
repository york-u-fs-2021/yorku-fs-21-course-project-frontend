import React, { useState } from 'react'

const AddAward = (props) => {
    const token = sessionStorage.getItem('token')
    
    const [awardName, setAwardName] = useState("")
    const [year, setYear] = useState("")
    const [description, setDescription] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    const formSubmit = async (event) => {
        event.preventDefault()
        event.persist()
        const response = await fetch(process.env.REACT_APP_API + '/resume/awards', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({awardName, year, description})
        })
        const payload = await response.json()
        if (response.status === 400) {
            setAlertContent(payload)
        } else {
            setAlertContent(null)
            alert(payload)
            resetForm()
            props.getAwardItems()
            resizePanel(event)
        }
    }

    const resetForm = () => {
        setAwardName("")
        setYear("")
        setDescription("")
    }

    function resizePanel(event){
        let panel = event.target.closest("section.panel")
        panel.style.maxHeight = (panel.scrollHeight + 50) + "px";
    }

    return (
        <div className="add-form">
            <form onSubmit={formSubmit}>
                <p>Fill out this form to add a new item to the award section of the resume.</p>
                <p style={{fontStyle: "italic"}}>All fields are required.</p>          
                <label htmlFor="awardEntry">Award Name: </label>            
                    <input style={{width: "50%"}} type="text" name="award" id="awardEntry" required value={awardName} onChange={e => setAwardName(e.target.value)}/>     
                <br/><br/>
                <label htmlFor="yearEntry">Year: </label>
                    <input style={{width: "50%"}} type="text" name="year" id="yearEntry" required value={year} onChange={e => setYear(e.target.value)}/>                       
                <br/><br/>
                <label htmlFor="awardDescription">Description:{String.fromCharCode(160)}
                    <textarea style={{verticalAlign: "text-top", width: "80%"}} rows="3" name="description" id="awardDescription" required value={description} onChange={e => setDescription(e.target.value)}></textarea>         
                </label>
                <br/><br/>
                <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                <button type="submit">Submit</button>         
            </form>
        </div>
    )
}

export default AddAward