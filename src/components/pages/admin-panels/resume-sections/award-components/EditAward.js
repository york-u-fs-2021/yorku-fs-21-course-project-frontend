import React, { useState, useEffect } from 'react'

const EditAward = (props) => {
    const token = sessionStorage.getItem('token')

    const [awardName, setAwardName] = useState("")
    const [year, setYear] = useState("")
    const [description, setDescription] = useState("")
    const [alertContent, setAlertContent] = useState(null)

    useEffect(() => {
        setAwardName(props.itemPick.awardName)
        setYear(props.itemPick.year)
        setDescription(props.itemPick.description)
    }, [props.itemPick])

    const updateAward = async () => {
        let itemID = props.itemPick.itemID
        const response = await fetch(`${process.env.REACT_APP_API}/resume/awards/${itemID}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({awardName, year, description})
        })
        const payload = response.json()
        if (response.status === 400) {
            setAlertContent(payload)
            return false
        } else {
            return true
        }
    }

    const formSubmit = async (event) => {
        event.preventDefault()
        let updated = await updateAward()
        if (!updated) return
        setAlertContent(null)
        props.getAwardItems()
        props.setItemPick({}) 
    }

    return (
        <div id="edit-award-modal" className={`${!props.itemPick.itemID ? "hidden" : ""}`}>
            <span className="close pointer" onClick={() => props.setItemPick({})}>×</span>
            <div className="award-modal-content">
                <form onSubmit={formSubmit}>
                    <p>Fill out this form to edit an award item.</p>
                    <p style={{fontStyle: "italic"}}>All fields are required.</p>          
                    <label htmlFor="editAward">Award Name: </label>            
                        <input style={{width: "50%"}} type="text" name="award" id="editAward" required value={awardName} onChange={e => setAwardName(e.target.value)}/>     
                    <br/><br/>
                    <label htmlFor="editYear">Year: </label>
                        <input style={{width: "50%"}} type="text" name="year" id="editYear" required value={year} onChange={e => setYear(e.target.value)}/>                       
                    <br/><br/>
                    <label htmlFor="editDescription">Description:{String.fromCharCode(160)}
                        <textarea style={{verticalAlign: "text-top", width: "80%"}} rows="3" name="description" id="editDescription" required value={description} onChange={e => setDescription(e.target.value)}></textarea>         
                    </label>
                    <br/><br/>
                    <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
                    <br/>
                    <button type="submit">Submit</button>         
                </form>
            </div>
        </div>
    )
}

export default EditAward