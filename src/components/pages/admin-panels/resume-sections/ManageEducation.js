import React, { useState, useEffect } from 'react'
import { Table } from 'reactstrap'
import AddEduItem from './education-components/AddEduItem'
import AddEduPoint from './education-components/AddEduPoint'
import EditEduItem from './education-components/EditEduItem'

const ManageEducation = () => {
    const token = sessionStorage.getItem('token')

    const [eduItems, setEduItems] = useState([])
    const [itemPick, setItemPick] = useState({})

    const getEduItems = async () => {
        const response = await fetch(process.env.REACT_APP_API + '/resume/education', {
            method: 'GET',
            mode: 'cors'
        })
        const data = await response.json()
        setEduItems(data)
    }

    const deleteEduItem = async (event) => {
        let itemID = event.target.dataset.key
        let confirmation = window.confirm("Deleted items cannot be retrieved. Are you sure you want to proceed?")
        if (confirmation){
            await fetch(`${process.env.REACT_APP_API}/resume/education/${itemID}`, {
                method: 'DELETE',
                mode: 'cors',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            getEduItems()
        }
    }

    const getEduItem = async (event) => {
        let itemID = event.target.dataset.key
        const response = await fetch(`${process.env.REACT_APP_API}/resume/education/${itemID}`, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        const data = await response.json()
        setItemPick(data)
    }

    useEffect(() => {getEduItems()}, [])

    return (
        <>
            <AddEduItem getEduItems={getEduItems}/>
            <AddEduPoint eduItems={eduItems}/>
            <Table responsive id="education-items">
                <thead>
                    <tr>
                        <th style={{padding: "0 10px"}}>Item ID</th>
                        <th>Credential</th>
                        <th>Date Range</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {eduItems.map(item => <tr key={item.itemID}><td style={{textAlign: "center"}}>{item.itemID}</td><td>{item.credentialName}</td><td>{item.dateRange}</td>
                    <td style={{minWidth: "5rem"}}><i className="fas fa-edit" title="Edit/View" data-key={item.itemID} onClick={e => getEduItem(e)}></i> | <i className="fas fa-trash-alt" title="Delete" data-key={item.itemID} onClick={e => deleteEduItem(e)}></i></td>
                    </tr>)}
                </tbody>
            </Table>
            <EditEduItem itemPick={itemPick} setItemPick={setItemPick} getEduItems={getEduItems}/>
            <div style={{marginBottom: "1rem"}}></div>
        </>
    )
}

export default ManageEducation