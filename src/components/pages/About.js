import React, { useEffect, useRef } from 'react'
import Grenfell from "./assets/aboutImages/GrenfellCampusLayout.png"
import DurhamCollege from "./assets/aboutImages/DurhamCollege.jpg"
import DowntownToronto from "./assets/aboutImages/downtownToronto.jpg"
import CNedgewalk from "./assets/aboutImages/CNTowerEdgewalk.jpg"
import LittleDetours from "./assets/aboutImages/LittleDetours.jpg"
import PotentialQuote from "./assets/aboutImages/greatestWaste.jpg"
import "../../styles/about.css"
import "../../styles/bubbly-button.css"

const About = () => {
    const animateButton = function(e) {
        let classes = e.target.classList;

        if (classes.contains('activePanel') && classes.contains('accordion')){
            e.target.firstElementChild.classList.remove('animate');    
            e.target.firstElementChild.classList.add('animate');
            setTimeout(function(){
                e.target.firstElementChild.classList.remove('animate');
            }, 1000);
        }
        if (classes.contains('about-h3') && e.target.parentElement.classList.contains('activePanel')){
            classes.remove('animate');
            classes.add('animate');
            setTimeout(function(){
                classes.remove('animate');
            }, 1000);
        }
    };

    const mainContainer = useRef()

    useEffect(() => {
        const accordions = Array.from(mainContainer.current.getElementsByClassName("accordion"))

        const togglePanel = function(e) {
            // Toggle between adding and removing the "active" class, to highlight the button that controls the panel
            this.classList.toggle("activePanel")
            // Toggle between hiding and showing the active panel
            let panel = this.nextElementSibling
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px"
            }
            animateButton(e)
        }

        const resizePanels = function() { //this function will make sure that all content in an active panel remains visible when the window is resized
            let activePanels = mainContainer.current.querySelectorAll(".activePanel + section.panel")
            activePanels.forEach(activePanel => activePanel.style.maxHeight = activePanel.scrollHeight + "px")
        }

        accordions.forEach(accordion => accordion.addEventListener("click", togglePanel))
        window.addEventListener("resize", resizePanels)

        return () => {
            accordions.forEach(accordion => accordion.removeEventListener("click", togglePanel))
            window.removeEventListener("resize", resizePanels)
        }
    })

    return (
        <main ref={mainContainer}>
            <h2>About Me</h2>
            <button className="accordion">
                <h3 className="center about-h3 bubbly-button">University <span></span></h3>
            </button>   
            <section className="panel">
                <figure className="fig-right">
                    <img src={Grenfell} alt="Grenfell Campus, Memorial University of Newfoundland" id="img-grenfell"/>
                    <figcaption className="center">
                        <i>A layout of Grenfell Campus, MUN around 2013.</i>
                    </figcaption>
                </figure>
                <p>
                    Through highschool I maintained an average of about 90% and graduated
                    with honours in 2009. I decided to go to Memorial University of Newfoundland
                    to study Environmental Science, with a major in chemistry. 
                    I started the 4-year long B.Sc. university program in the same year of 2009 and completed it in 2013. 
                    I finished with a GPA of 3.94 out of 4.
                </p>
                <p>It was after my third year of university that I started doing yoga. I just happened to see an advertisement for it
                    and started following along with a class on tv. Then I got several yoga DVDs to practice more and books to learn more.
                    It was good to have yoga during my fourth year of university.
                </p>
                <p>After graduating, I didn't realize how difficult it would be to get work in my
                    field of study in Newfoundland. It was around this time that I became more dedicated to my yoga practice and started
                    practicing daily, which I managed to keep up for at least one full year.
                    I moved to St. John's and lived there for about six months.
                </p>
                <p>
                    I worked at a Sobey's for a while. It was during this time that I started using Excel spreadsheets 
                    to track my money: my income, expenses, and how the two balanced out (a practice I continue to do today). I found that making minimum wage would
                    not be enough, especially as my hours were being reduced, and that slowly my savings would be used up. Realizing it
                    wasn't a sustainable situation, I moved back home and got a job at a new Tim Horton's that opened up near my 
                    hometown. It was better but it wasn't what I went to university for.
                </p>
            </section>
            <hr/>

            <button className="accordion">
                <h3 className="center about-h3 bubbly-button">College <span></span></h3>
            </button>
            <section className="panel">
                <figure className="fig-left">
                    <img src={DurhamCollege} alt="Durham College entrance" id="img-durham"/>
                    <figcaption className="center">
                        <i>Durham College.</i> 
                    </figcaption>
                </figure>
                <p>After working at Tim Horton's for some time, I started looking up other education and chose a 1-year advanced diploma in Chemical Engineering Technology
                    at Durham College in Oshawa, Ontario. I picked this program for a few reasons:</p>
                <ul className="ul-indent-left">
                    <li>to learn more about chemistry</li>
                    <li>it had a work term, so I could get some experience</li>
                    <li>to experience being in another province</li>
                </ul>               
                <p>
                    Up to this point I had never been on a plane and had never been outside of Newfoundland. While the work term turned out to be
                    mostly job-shadowing, overall it was a very good experience.
                </p>
                <p>I returned to Newfoundland ready to get back into job searching in full earnestness, now knowing how difficult it would be despite doing well academically. 
                    There was one point where I borrowed my father's car for a weekend and drove across the province putting out resumes. Incidentally, at some point in my job search
                    I ended up working at the same Tim Horton's again to have some income while looking for work in my field of study. Eventually, I expanded my job search beyond Newfoundland 
                    to other provinces in Canada. Wanting to stay in Newfoundland I was reluctant to do this but the job opportunities just weren't there.
                    While working at this Tim Horton's was good and I got along well with my co-workers, spending the rest of my life working there wasn't quite ideal.
                    I must have sent out about 150 resumes before I got my first interview. Luckily that one was enough and I got a job with Digital Specialty Chemicals,
                    a chemical manufacturing company located in Scarborough, ON.
                </p>
            </section>

            <hr/>
            <br/>
            <img id="imgLD" src={LittleDetours} alt="Beautiful sunset on a walkway next to Lake Ontario"/>
            <br/>
            <hr/>

            <button className="accordion">
                <h3 className="center about-h3 bubbly-button">Greater Toronto Area <span></span></h3>
            </button>
            <section className="panel">
                <figure className="fig-right">
                    <img src={DowntownToronto} alt="Buildings in downtown Toronto" id="img-dt-toronto"/>
                    <figcaption className="center caption-toronto">
                        Toronto - a bigger city with bigger buildings.
                    </figcaption>
                </figure>
                <p>The size of the GTA relative to St.John's took some adjustment but it didn't bother me much. Actually,
                    I liked that there was more to do, public transit was way better, and many things were more accessible.
                    Work as a production chemist paid decently, though it was mostly manual labour. Some materials I worked with were
                    pyrophoric (meaning the chemical ignites in contact with oxygen). I worked there for about a year but, after a couple safety incidents, decided
                    it was not something I wanted to do long-term.
                </p>
                <p>Back when I was in college I had taken a yoga series through the campus's recreation centre. It inspired me and made me want to 
                    become a yoga teacher one day. After leaving Digital Specialty Chemicals I was positioned well (financially and location-wise) to take a yoga teacher training.
                    It was during that summer that I took a 200 hour training at Yogaspace in Toronto.
                    Having practiced yoga for a few years and reading several books, I was well prepared and a lot of the material
                    came naturally. Anatomy was completely new to me though so I learned a lot from that section.
                </p>
                <p>After completing my training, I started looking for work in the fitness industry (including teaching as well of course).
                    I got two part time jobs as a studio coordinator, one at Pur Yoga studio and one at Merrithew Stott Pilates studio,
                    while also teaching yoga classes when I got the opportunity.
                    While I earned a bit less than I did while working as a production chemist, I made more than some chemistry-related jobs I saw in past job searches, 
                    some of which were minimum wage or only slightly higher. Also, I would have regretted not having a go at something
                    I was really passionate about. For a couple years things were going well.
                </p>
            </section>
            <hr/>

            <button className="accordion">
                <h3 className="center about-h3 bubbly-button">Pandemic Times <span></span></h3>
            </button>
            <section className="panel">
                <p>Eventually, around the end of February 2020, I had decided to leave my job at Pur Yoga. I had several reasons for leaving but two of them were
                    how the studio was being managed and I wasn't getting the teaching opportunities there that I had hoped for. I was going to
                    rent out some space at a church to teach some classes; I had things lined up to start but then the pandemic struck and things
                    went into lockdown. At my studio coordinator job at Merrithew I was temporarily laid off and things turned into a waiting game to return. I had mentioned earlier
                    that I kept track of my money using Excel spreadsheets. Another good habit I made was keeping a stockpile of savings because you never know what the hell
                    can happen in life.
                </p>
                <p>Within about a month of the pandemic, my home province of Newfoundland banned non-essential travel to the province. To travel there you would need to get an exemption,
                    something which I think some people were arbitrarily denied. So I waited a few more months for things to reopen in the GTA but there was no sign of that happening,
                    especially for the fitness industry. Eventually, in the summer, when covid cases were lower, I applied for a travel exemption to move back home and I consider 
                    myself lucky to have been approved. I could have kept waiting in hopes of things reopening but with talk of a second wave of covid<sup>1</sup> potentially coming in the fall
                    I decided it would be better to return home if possible, which I did near the end of August.
                </p>
                <p>Before leaving Toronto I made the most of my remaining time there. I took the opportunity to do some things I hadn't done before and may not get to do again (at least for 
                    a long time). I went to the Royal Ontario Museum (ROM), spent about 5 hours there and still didn't see everything inside; I went to the Aga Khan Museum (I'd recommend
                    going here before the ROM as it is smaller); walked around High Park for the better part of a day; went inside the CN Tower and did the edgewalk on top of it.
                </p>
                <figure className="center">
                    <img src={CNedgewalk} alt="Edgewalk on top of the CN Tower" id="img-CNedgewalk"/>
                    <figcaption>
                        <i>A pic of me on the edge of the CN Tower.</i>
                    </figcaption>
                </figure>
                <p>After arriving home and quarantining for two weeks I had to figure out what to do next. The fitness industry was hit hard by the pandemic<sup>2</sup>. It would have been difficult even
                    before to make a living from mostly teaching classes, now it seemed less likely. For chemistry-related work the job prospects and income didn't seem that great, as I had learned from past job searches.
                    Newfoundland's job market in particular was still bad and now made worse by the pandemic with some large company shutdowns. One good thing at least is that NL has some of the lowest covid case numbers
                    in Canada. In the end I decided to pivot to web development for a few reasons:</p>
                <ul> 
                    <li>When I worked at Pur Yoga I had to learn how to use software I had never used before such as MindBody Online, Wellness Living, and Squarespace.
                    The latter was used to edit the studio's website. I'm fairly tech savvy and I liked working on these things. Taking the next step forward seemed appropriate.</li>
                    <li>Job prospects seem to be good with more companies making the transition to being online because of the pandemic.</li>
                    <li>Salaries look great.</li>
                    <li>The higher likelihood of getting to work remotely means I could probably stay in NL.</li>
                </ul>       
                <p>
                    I researched some online course options and decided on York University's certificate in full-stack web development which started
                    in January 2021. Though I've been out of work since March 2020, my hopes are that this program will help me get back into the workforce and make a good living.
                    If I can find work that combines what I learn in web development with anything else I've previously learned that would be even better.
                </p>
                <p style={{fontWeight: "bold"}}>Notes:</p>
                <p><sup>1 </sup>I lost count of how many waves of Covid there has been.</p>
                <p><sup>2 </sup> 
                    Pur Yoga, where I used to work, closed down at least physically and transitioned to online offerings. 
                    I've heard of other yoga studios closing too, such as The Yoga Sanctuary which was one of the oldest yoga studios in Toronto.
                </p>
            </section>
            <hr/>

            <button className="accordion">
                <h3 className="center about-h3 bubbly-button">2022 Onwards <span></span></h3>
            </button>
            <section className="panel">
                <p>
                    In December 2021 I finished the course in full-stack web development. My job search to this date has been difficult and fruitless.
                    Initially, I focused on finding remote work with no luck. After expanding my search (a few months in) to include web/software development roles anywhere onsite
                    in Canada I still could not get anywhere. In August 2022 I started work as a cashier at a grocery store in my hometown, while continuing to look for better work.
                    Through 2022 I applied to over 300 jobs.
                </p>
                <p>
                    In 2023 I am expanding my job search again to now include any roles, remote or onsite across Canada, that are related to
                    web/software development, fitness, administration, and/or chemistry (i.e. any roles that I have some background in).
                </p>
                <p>
                    A common thread in my life has been doing well academically but never being given a chance...
                </p>
            </section>

            <hr/>
            <br/>
            <img id="potentialQuote" src={PotentialQuote} alt="A quote which says 'The greatest waste...is failure to use the abilities of people...to learn about their frustrations and about the contributions that they are eager to make."/>
            <br/>
            <hr/>
            <br/>
            <p id='about-update-txt'>
                The content of this page was last updated on January 14th, 2023.
            </p>
        </main>
    )
}

export default About